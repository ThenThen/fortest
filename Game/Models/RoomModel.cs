﻿using System;
using System.Collections.Generic;

namespace Game.Models {
    public class RoomModel {
        public long RoomId { get; set; }
        public string OwnerUser { get; set; }
        public List<String> Gamers { get; set; } 
        public int GamersCount { get; set; }
        public bool IsGameStart = false;
    }
}
﻿using System;

namespace Game.Models {
    public class GamerModel {
        public string ConnectionId { get; set; }
        public String UserName { get; set; }
        public long RoomId { get; set; }
        public DateTime TimeOut { get; set; }
    }
}
﻿using System.Web;
using System.Web.Optimization;

namespace Game
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jcarousel").Include(
                        "~/Scripts/jquery.jcarousel.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Content/Neon.css",
                      "~/Content/button-style.css"));

            bundles.Add(new ScriptBundle("~/bundles/triangle").Include(
                      "~/Scripts/triangle.draw.js"));
            bundles.Add(new ScriptBundle("~/bundles/util").Include(
          "~/Scripts/util.js"));

            bundles.Add(new ScriptBundle("~/bundles/signalR").Include(
          "~/Scripts/jquery.signalR-2.2.0.js"));

            bundles.Add(new ScriptBundle("~/bundles/burn").Include(
                        "~/Scripts/jquery.burn.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/circle").Include(
                        "~/Scripts/circle-progress.js"));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Game.Models;
using Microsoft.AspNet.SignalR;

namespace Game.Hubs
{
    public class MainPageHub : Hub
    {
        static List<GamerModel> Gamers = new List<GamerModel>();
        static List<RoomModel> Rooms = new List<RoomModel>();
        static Dictionary<String,DateTime> Ban = new Dictionary<String, DateTime>();

        public void Send(string name, string msg) {
            Clients.All.addMessage(name, msg);
        }

        public void Connect() {
            var id = Context.ConnectionId;
            var name = Context.User.Identity.Name;
            if (!Gamers.Any(x => x.ConnectionId == id) && !Gamers.Any(x => x.UserName == name)) {
                Gamers.Add(new GamerModel { ConnectionId = id, UserName = name});
                Clients.AllExcept(id).onNewUserConnected(id, name);
            }
            Clients.Caller.onConnected(id, name, Gamers, Rooms);
        }

        public void CreateRoom(String ownerId, int gamersCount) {
            var id = Context.ConnectionId;
            var currentUserName = Context.User.Identity.Name;
            if (Ban.ContainsKey(currentUserName)) {
                if (TimeSpan.FromTicks(DateTime.Now.Ticks - Ban[currentUserName].Ticks).TotalSeconds > 60) {
                    Ban.Remove(currentUserName);
                } else {
                    return; 
                    
                }
            }
            if (Gamers.FirstOrDefault(x => x.ConnectionId.Equals(id)) == null)
            {
                var firstConnection = Gamers.FirstOrDefault(x => x.UserName.Equals(Context.User.Identity.Name));
                if (firstConnection != null)
                {
                    id = firstConnection.ConnectionId;
                }
            }
            if (!Rooms.Any(x => x.OwnerUser.Equals(id)))
            {
                Rooms.Add(new RoomModel{ OwnerUser = id, GamersCount = gamersCount, Gamers = new List<string>() {id} });
                var user = Gamers.FirstOrDefault(x => x.ConnectionId == ownerId);
                if (user == null) {
                    user = Gamers.FirstOrDefault(x => x.UserName.Equals(Context.User.Identity.Name));
                }
                var name = user.UserName;
                Clients.Caller.onRoomCreated(id,name);
                Clients.Caller.onNewRoomAdded(id, name, gamersCount);
                Clients.AllExcept(id).onNewRoomAdded(id, name, gamersCount);
            }
        }

        public void ExitFromRoom(String userId) {
            var room = Rooms.FirstOrDefault(x => x.Gamers.Contains(userId));
            if (room != null) {
                var isStart = room.IsGameStart;
                if (room.OwnerUser.Equals(userId)) {
                    Rooms.Remove(Rooms.FirstOrDefault(x => x.OwnerUser.Equals(userId)));
                    Clients.Caller.onRoomRemoved(userId);
                    Clients.AllExcept(userId).onRoomRemoved(userId);
                    Clients.Clients(room.Gamers.Where(x => !x.Equals(userId)).ToList()).onComplete(room.OwnerUser);
                    Rooms.Remove(Rooms.FirstOrDefault(x => x.OwnerUser.Equals(userId)));
                }
                room.Gamers.Remove(userId);
                if (isStart) {
                    Ban.Add(Context.User.Identity.Name,DateTime.Now);
                } else {
                    Clients.All.onRoomState(room.OwnerUser, "Набор участников...");
                }

                Clients.Clients(room.Gamers.Where(x => !x.Equals(userId)).ToList()).onGamerExit(userId);
            }
        }

        public void GameStart() {
            var id = Context.ConnectionId;
            if (Gamers.FirstOrDefault(x => x.ConnectionId.Equals(id)) == null)
            {
                var firstConnection = Gamers.FirstOrDefault(x => x.UserName.Equals(Context.User.Identity.Name));
                if (firstConnection != null)
                {
                    id = firstConnection.ConnectionId;
                }
            }
            var room = Rooms.FirstOrDefault(x => x.OwnerUser.Equals(id) && x.Gamers.Count == x.GamersCount);
            if (room != null) {
                room.IsGameStart = true;
                Clients.All.onRoomState(id, "Идет игра.");
                var gamers = Gamers.Where(x => room.Gamers.Contains(x.ConnectionId)).Select(x=>x.ConnectionId).ToList();
                Clients.Clients(gamers).onStartGame();
            }
        }

        public void GameStop() {
            var id = Context.ConnectionId;
            if (Gamers.FirstOrDefault(x => x.ConnectionId.Equals(id)) == null) {
                return;
            }
            var room = Rooms.FirstOrDefault(x => x.Gamers.Contains(id));
            if (room != null &&
                room.IsGameStart) {
                room.IsGameStart = false;
                Clients.Clients(room.Gamers.ToList()).onComplete(room.OwnerUser);
            }
        }

        public void Participate(String ownerId)
        {
            var id = Context.ConnectionId;
            var currentUserName = Context.User.Identity.Name;
            if (Ban.ContainsKey(currentUserName))
            {
                if (TimeSpan.FromTicks(DateTime.Now.Ticks - Ban[currentUserName].Ticks).TotalSeconds > 60)
                {
                    Ban.Remove(currentUserName);
                }
                else
                {
                    return;

                }
            }
            if (Gamers.FirstOrDefault(x => x.ConnectionId.Equals(id)) == null) {
                return;
            }
            var room = Rooms.FirstOrDefault(x => x.OwnerUser == ownerId);

            if (room != null &&
                room.Gamers.Count < room.GamersCount) {
                room.Gamers.Add(id);
            }

            if(room.Gamers.Count >= room.GamersCount) {
                Clients.All.onRoomState(ownerId, "Набор участников закончен.");
            }

            var gamers = room.Gamers;//Gamers.Where(x => room.Gamers.Contains(x.ConnectionId)).ToList();
            var GamersList = Gamers.Where(x => gamers.Contains(x.ConnectionId) && !x.ConnectionId.Equals(id)).ToList();
            Clients.Caller.onRoomParticipate(GamersList);
            var name = Gamers.FirstOrDefault(x => x.ConnectionId.Equals(id)).UserName;
            //var GamersList = Gamers.Where(x => !x.ConnectionId.Equals(id)).Select(x=>x.ConnectionId).ToList();
            if (name != null) {
                Clients.Clients(gamers)
                    .participateNewGamer(id, name);
            }

        }

        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Gamers.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Gamers.Remove(item);
                var id = Context.ConnectionId;
                var firstConnection = Gamers.FirstOrDefault(x => x.UserName.Equals(Context.User.Identity.Name));
                if (firstConnection != null)
                {
                    id = firstConnection.ConnectionId;
                }
                Clients.All.onUserDisconnected(id, item.UserName);
                ExitFromRoom(id);
            }

            return base.OnDisconnected(stopCalled);
        }

    }
}
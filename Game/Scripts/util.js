﻿$(function () {

    var clock = $('.my-clock').FlipClock({
    });
    clock.setTime(59);
    clock.setCountdown(true);
    clock.stop();
    // Ссылка на автоматически-сгенерированный прокси хаба
    var gameServer = $.connection.mainPageHub;
    // Объявление функции, которая хаб вызывает при получении сообщений
    gameServer.client.onRoomCreated = function (id, name) {
        $("#room").hide();
        $("#game-room").show();
        var lastElem = $('.circle-field');
        lastElem.append('<div class="circle" id="' + id + '">' + htmlEncode(name).replace(";", "") + '</div>');
        $(".startBtn").show();
        $(window).trigger('resize');

    };

    // Функция, вызываемая при подключении нового пользователя
    gameServer.client.onConnected = function (id, userName, allUsers, allRooms) {
        for (i = 0; i < allUsers.length; i++) {
            AddUser(allUsers[i].ConnectionId, allUsers[i].UserName);
        }
        for (i = 0; i < allRooms.length; i++) {
            AddRoom(allRooms[i].OwnerUser, null, allRooms[i].GamersCount);
        }
        $(".create-users2").on("click", function () {
            gameServer.server.createRoom(id, 2);
        });
        $(".create-users4").on("click", function () {
            gameServer.server.createRoom(id, 4);
        });
        $(".exitBtn").on("click", function () {
            $("#room").show();
            $("#game-room").hide();
            $('.circle').remove();
            gameServer.server.exitFromRoom(id);
            clock.reset();
            clock.setTime(59);
        });
        $('.startBtn').on("click", function () {
            gameServer.server.gameStart();
        });
    }

    gameServer.client.onComplete = function(id) {
        $("#room").show();
        $("#game-room").hide();
        $('.circle').remove();
        gameServer.server.exitFromRoom(id);
        clock.reset();
        clock.setTime(59);
    }

    gameServer.client.onGamerExit = function (id) {
        $('[class = circle][id = '+id+']').remove();
    }


    gameServer.client.onNewRoomAdded = function (id, name, gamersCount) {
        AddRoom(id, name, gamersCount);
    }


    gameServer.client.onRoomState = function (id, message) {
        $('[class = roomForm][id =' + id + ' ]').html('<div>'+message+'</div>');
    }

    function AddRoom(id, name, gamersCount) {
        var elem = $("." + gamersCount + "-users").find("#onlineUsersList");
        elem.append('<li class = "roomForm" id="' + id + '"><div>Набор участников...</div></li>');
        $('[class = roomForm][id =' + id + ' ]').on("click", function () {
            gameServer.server.participate($(this).attr("id"));
        });
        $(window).trigger('resize');
    }

    // Добавляем нового пользователя
    gameServer.client.onNewUserConnected = function (id, name) {

        AddUser(id, name);
    }


    gameServer.client.onRoomParticipate = function (allUsers) {
        for (i = 0; i < allUsers.length; i++) {
            AddUserToRoom(allUsers[i].ConnectionId, allUsers[i].UserName);
        }
    }

    gameServer.client.onStartGame = function () {
        clock.start();
        setTimeout(function() { gameServer.server.gameStop();}, 60000);
    }

    gameServer.client.participateNewGamer = function (id, name) {
        AddUserToRoom(id, name);
    }

    function AddUserToRoom(id, name) {
        $("#room").hide();
        $("#game-room").show();
        var lastElem = $('.circle-field');
        lastElem.append('<div class="circle" id="' + id + '">' + htmlEncode(name).replace(";", "") + '</div>');

        $(window).trigger('resize');
    }



    gameServer.client.onRoomRemoved = function (id) {
        $('[class = roomForm][id =' + id + ' ]').remove();
    }

    // Удаляем пользователя
    gameServer.client.onUserDisconnected = function (id, userName) {
        $('#' + id).remove();
    }

    gameServer.client.onBan = function () {
        $(".create-users2").prop("disabled", true);
        setTimeout(function () { $(".create-users2").prop("disabled", false); }, 60000);
    }

    // Открываем соединение
    $.connection.hub.start().done(function () {
        gameServer.server.connect();
    }
    );

    // Кодирование тегов
    function htmlEncode(value) {
        var encodedValue = $('<div />').text(value).html();
        return encodedValue;
    }

    //Добавление нового пользователя
    function AddUser(id, name) {

        var lastElem = $('#onlineUsersList').find('li').last();
        if (lastElem.find('div').length < 3) {
            lastElem.append('<div id="' + id + '">' + htmlEncode(name) + '</div>');
        } else {
            $('#onlineUsersList').append('<li><div id="' + id + '">' + htmlEncode(name) + '</div></li>');
        }
        $(window).trigger('resize');
    }
})
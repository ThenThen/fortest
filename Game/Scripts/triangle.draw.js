﻿var canvas = document.getElementById('triangle');
    var context = canvas.getContext('2d');

    context.beginPath();
    context.moveTo(10, 25);
    context.lineTo(40, 10);
    context.lineTo(40, 40);
    context.lineTo(10, 25);

    context.closePath();
    context.shadowColor = "rgba(0, 0, 0, 0.4)";
    context.shadowBlur = 7;
    context.shadowOffsetX = 2;
    context.shadowOffsetY = 5;
    context.fillStyle = "rgba(132, 28, 255, 0.8)";
    context.fill();
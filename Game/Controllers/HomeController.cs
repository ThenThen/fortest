﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Game.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated) {
                return View();
            } else {
                return RedirectToAction("LogIn", "Account", new { area = "" });
            }
        }

        public ActionResult GameRoom()
        {
            if (Request.IsAuthenticated)
            {
                return View();
            }
            else
            {
                return RedirectToAction("LogIn", "Account", new { area = "" });
            }
        }
    }
}